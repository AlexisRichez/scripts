#!/bin/sh

if [ $(id -u) != 0 ] ; then
    echo "Root privileges are required"
    echo "Please run 'sudo $0' or log in as root, and rerun $0"
    exit 1
fi

wget https://raw.githubusercontent.com/jeedom/core/stable/install/install.sh
chmod +x install.sh
./install.sh